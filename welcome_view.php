




<html>
    <head>
        <title>Register pages</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!------ Include the above in your HEAD tag ---------->
        <style>

            body {
                background:#ece2e2;
            }
            #login {
                -webkit-perspective: 1000px;
                -moz-perspective: 1000px;
                perspective: 1000px;
                margin-top:50px;
                margin-left:30%;
            }
            .login {
                font-family: 'Josefin Sans', sans-serif;
                -webkit-transition: .3s;
                -moz-transition: .3s;
                transition: .3s;
                -webkit-transform: rotateY(40deg);
                -moz-transform: rotateY(40deg);
                transform: rotateY(40deg);
            }
            .login:hover {
                -webkit-transform: rotate(0);
                -moz-transform: rotate(0);
                transform: rotate(0);
            }
            .login article {

            }
            .login .form-group {
                margin-bottom:17px;
            }
            .login .form-control,
            .login .btn {
                border-radius:0;
            }
            .login .btn {
                text-transform:uppercase;
                letter-spacing:3px;
            }
            .input-group-addon {
                border-radius:0;
                color:#fff;
                background:#f3aa0c;
                border:#f3aa0c;
            }
            .forgot {
                font-size:16px;
            }
            .forgot a {
                color:#333;
            }
            .forgot a:hover {
                color:#5cb85c;
            }

            #inner-wrapper, #contact-us .contact-form, #contact-us .our-address {
                color: #1d1d1d;
                font-size: 19px;
                line-height: 1.7em;
                font-weight: 300;
                padding: 50px;
                background: #fff;
                box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
                margin-bottom: 100px;
            }
            .input-group-addon {
                border-radius: 0;
                border-top-right-radius: 0px;
                border-bottom-right-radius: 0px;
                color: #fff;
                background: #f3aa0c;
                border: #f3aa0c;
                border-right-color: rgb(243, 170, 12);
                border-right-style: none;
                border-right-width: medium;
            }

        </style>
    </head>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">MENU</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

<?php //if($this->session->userdata('id'))
{ ?>
<li><a href="<?= ('log_out'); ?>"class="btn btn-danger">Logout</a></li>
<li><a href="<?= ('welcome_fetch'); ?>"class="btn btn-danger">View</a></li>


<?php }?>
 
</nav>

    <body ng-app="myApp" ng-controller="namesCtrl">
        <table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</table>
    </body>

        <script>

                    angular.module('myApp', []).controller('namesCtrl', function ($scope, $http) {
                        $scope.data = {};
                       
                        $scope.welcome_data = function () {
                            $http({
                                method: 'get',
                                url: 'welcome_fetch', //define on routes.php
//                                data: $scope.data,
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }).then(function (jsondata) {
                                console.log(jsondata);
//                                if (jsondata.data.type == 'true') {
//                                    alert(jsondata.data.msg);
//                                    $scope.data = {};
////                                      $window.location.href = 'login_auth';//redirect page
//                                } else {
//                                    alert(jsondata.data.msg);
//                                }
                            })
                        };
                        $scope.welcome_data();
                    });
        </script>
    </body>



</html>

