<!DOCTYPE html>

<html>
<head>
	<title>CRUD APPLICSTION IN CI</title>


<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


</head>

<!-- //////////////////////BODY////////////////////// -->
<body>
	<div class="navbar navbar-dark bg-dark">
		<div class="container">
			<a href="#" class="navbar-brand">CRUD APPLICATION  </a>
			
		</div>
		
	</div>

	<div class="container" style="padding-top: 10px">
		
			<div class="row">

				 <div class="col-md-6"><h3>VIEW USER</h3></div>
		 <div class="col-md-6" text-right>
				<td>
						<a href="<?php echo base_url().'index.php/user/create'?>" class ="btn btn-primary">Create</a>
					</td>


	 </div>
				
			</div>
			
		

		 
		<hr>
		<div class="row">
			<div class="col-md-8">
				<table class="table table-striped">
					<tr>
					<th>Id</th>
					<th>Name</th>
					<th>Email</th>
					<th width="60">Edit</th>
					<th width="100">Delete</th>
					</tr>
<!--==========================hoe to show data==================================  -->

				<?php if(!empty($users)) {foreach($users as $user){?>
		
					<tr>
						
						    <td> <?php echo $user['user_id'] ?></td>
							<td> <?php echo $user['name'] ?></td>
						    <td> <?php echo $user['email'] ?></td>
							
					<td>
						<a href="<?php echo base_url().'index.php/user/edit'.$user['user_id']?>" class ="btn btn-primary">Edit</a>
					</td>

						<td>
						<a href="<?php echo base_url().'index.php/user/delete'.$user['user_id']?>" class= "btn btn-danger">Delete</a>
					</td>
					</tr>
			<?php	 } } else {?>
						<tr>
							<td colspan="5"> Record not found</td>
						</tr>

			<?php } ?>
					
				</table>
				
			</div>
			
		</div>
	
	</div>

</body>
</html>