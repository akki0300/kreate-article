<?php include('header.php');?>


<div class="container">
    <h1>Admin form</h1>
<!-- =============================================== -->
<!-- this block use in showing login_failed msg  by using $this->session->flashdata-->

<div class="container" style="margin-top: 20px">
  <?php if($error=$this->session->flashdata('login_failed')): ?>

  <div class="row">
    <div class="col-lg-6">
      <div class="alert alert-danger">

        <?= $error; ?>
      </div>
      
    </div>
    
  </div>
<?php endif; ?>
  
</div>







<!-- ==================================================== -->
<?php echo form_open('Admin/login');?>


	
<div class="row">
  <div class="col-lg-6">
    <div class="form-group" >
    <label> User name:</label><br>
  <!-- <input type="text" name="email" value="email"> -->
<?php echo form_input(['class'=>'form-control','placeholder'=>'Enter user name','name'=>'uname','value'=>set_value('uname')]); ?>
</div>

  </div>
  <div class="col-lg-6">
      <?php echo form_error('uname'); ?>
  </div>
  
</div>

<div class="row">
<div class="col-lg-6">
<div class="form-group">
  
  <label>Password:</label> <br>
 <?php echo form_input(['class'=>'form-control','type'=>'password','placeholder'=>'Enter password ','name'=>'pass','value'=>set_value('pass')]); ?>
</div>
 </div>
  <div class="col-lg-6">
      <?php echo form_error('pass'); ?>
  </div>

  
</div>

  <br>
 <?php echo form_submit(['type'=>'submit','class'=>'btn btn-primary','value'=>'submit']); ?>

 <?php echo form_reset(['type'=>'reset','class'=>'btn btn-primary','value'=>'reset']); ?>
 <?php echo anchor('admin/register','Sign up?','class="link-class"') ?>
  
  




<?php include('footer.php');?>

