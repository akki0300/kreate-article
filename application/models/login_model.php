<?php

class login_model extends CI_model
{
	
  ///////////////////////for user login authingation /////////////////////

	function isvalidate($username,$password)
	{
		$q=$this->db->where(['username'=>$username,'password'=>$password])
					->get('users');

					// echo "<pre>";
					// print_r($q->result());
					// die;

   if ( $q->num_rows())
    {
   	

      return  $q->row()->id;

   } 
   else 
   {
   	
     return false;

   }
   

	}
	public function articallist($limit,$offset)
	{
		$id=$this->session->userdata('id');
		$q=$this->db->select()
					->from('article')
					->where(['userid'=>$id])
          ->limit($limit,$offset)
					->get();
					return $q->result();
					//print_r($q->result());die;

	}
  public function num_rows() 
    {
        $id=$this->session->userdata('id');

          $q=$this->db->select()
          ->from('article')
          ->where(['userid'=>$id])
          ->get();
          return $q->num_rows();
          //print_r($q->result());die;
    }

 public function add_articles($array)
  {
     return $this->db->insert('article',$array);
  }

// //////////////////////////////////////for sign up ///////////////
  public function add_user($array)
  {
     return $this->db->insert('users',$array);
  }

    public function del($id)
  {
     return $this->db->delete('article',['id'=>$id]);
  }
   public function edit_article($articleid)

  {
   
     $q=$this->db->select(['article_title','article_body','id'])
                    ->where('id',$articleid)
                    ->get('article');
                    return $q->row();
  }
/////////////////////////////////for update///////////////

  public function update_article($articleid,$article)
   {
    
    $q= $this->db->where('id',$articleid)
            ->update('article',$article);
            return $q;

  }

  public function all_article_counts()
  {
    $q=$this->db->select()
          ->from('article')
          ->get();
           return $q->num_rows();
  }

  public function all_articlelist($limit,$offset)
  {

    $q=$this->db->select()
          ->from('article')
          
          ->limit($limit,$offset)
          ->get();
          return $q->result();

  }
}
       
       ?>