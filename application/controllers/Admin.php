

<?php

class Admin extends Akash_Controller {

    public function __construct() {
        parent::__construct();
    }

    // ///////////////////////////////login code//////////////////////////////


    function login() {

        $this->form_validation->set_rules('uname', 'User name', 'required|alpha');
        $this->form_validation->set_rules('pass', 'Password', 'required|max_length[12]');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>'); //it is use for red msg


        if ($this->form_validation->run()) {

            $uname = $this->input->post('uname');
            $pass = $this->input->post('pass');
            $this->load->model('login_model');

            $id = $this->login_model->isvalidate($uname, $pass);
            // echo $id;
            // die;
            if ($id) {

                $this->load->library('session');
                $this->session->set_userdata('id', $id);
                return redirect('admin/welcome');
            } else {
                $this->session->set_flashdata('login_failed', 'Invalid Username/password');
                return redirect('admin/login');
            }
        } else {

            $this->load->view('Admin/login');
        }
    }

//////////////////////////////////////welcome///////////////////////////////////////////////

    public function welcome() {

        $this->load->model('login_model', 'ar');
//=================pagination start============
        $this->load->library('pagination');

        $config = [
            'base_url' => base_url('admin/welcome'),
            'per_page' => 3,
            'total_rows' => $this->ar->num_rows(),
            'full_tag_open' => "<ul class='pagination'>",
            'full_tag_close' => "</ul>",
            'next_tag_open' => "<li>",
            'next_tag_close' => "</li>",
            'prev_tag_open' => "<li>",
            'prev_tag_close' => "</li>",
            'num_tag_open' => "<li>",
            'num_tag_close' => "<li>",
            'cur_tag_open' => "<li class='active'><a>",
            'cur_tag_close' => "</a></li>"
        ];


        $this->pagination->initialize($config);
        $articals = $this->ar->articallist($config['per_page'], $this->uri->segment(3));

        //=================pagination ends============
        $this->load->view('admin/dashboard', ['articals' => $articals]);
    }

/////////////	//////	// ////register///////////////////////////////////////

    public function register() {
        $this->load->view('Admin/register');
    }

////////////////////////////////////////////////////////////////////////////		
    public function adduser() {//this function use for adding article


        $this->load->view('admin/add_article');
    }

    //////////////////////add article and image upload///////////////////////////////

    public function uservalidation() {

// echo "<pre>";
// print_r($_POST);

        $config = [
            'upload_path' => './upload/',
            'allowed_types' => 'gif|jpg|png'
        ];
        $this->load->library('upload', $config);
        if ($this->form_validation->run('add_article_rules') && $this->upload->do_upload('userfile')) {
            $post = $this->input->post();
            $post['image_path'] = base_url('upload/' . ($this->upload->data('file_name'))); //it give the full path of image
            // print_r($post['image_path']);die;
            $this->load->model('login_model', 'lg');

            if ($this->lg->add_articles($post)) {

                $this->session->set_flashdata('msg', 'Articles added successfully');
                $this->session->set_flashdata('msg_class', 'alert-success');
            } else {
                $this->session->set_flashdata('msg', 'Articles not added Please try again!!');
                $this->session->set_flashdata('msg_class', 'alert-danger');
            }
            return redirect('admin/welcome');
        } else {

            $upload_error = $this->upload->display_errors();
            print_r($upload_error);
            $this->load->view('admin/add_article', compact('upload_error')); //compact PHP KA FINCTION HAI
        }
    }

////////////////////////Edit/////////////////////////////////////			

    public function edituser($id) {

        $this->load->model('login_model');
        $article = $this->login_model->edit_article($id);
        $this->load->view('admin/edit_article', ['article' => $article]);
    }

////////////////////////update///////////////////////////////

    public function updatearticle($articleid) {

        if ($this->form_validation->run('add_article_rules')) {//add_article_rules , this is config form_validation.php
            $post = $this->input->post();
            //$articleid=$post['articleid'] ;

            $this->load->model('login_model', 'editupdate');
            if ($this->editupdate->update_article($articleid, $post)) {

                $this->session->set_flashdata('msg', 'Articles update successfully');
                $this->session->set_flashdata('msg_class', 'alert-success');
            } else {
                echo "string";
                die;
                $this->session->set_flashdata('msg', 'Articles not updatePlease try again!!');
                $this->session->set_flashdata('msg_class', 'alert-danger');
            }
            return redirect('admin/welcome');
        } else {
            $this->load->view('admin/add_article');
        }
    }

///////////////////////////////for delete/////////////////////////////////////////		

    public function deluser() {

        $id = $this->input->post('id');
        $this->load->model('login_model', 'useradd');
        if ($this->useradd->del($id)) {
            $this->session->set_flashdata('msg', 'Articles Delete successfully');
            $this->session->set_flashdata('msg_class', 'alert-success');
        } else {
            $this->session->set_flashdata('msg', 'Please try again not!!');
            $this->session->set_flashdata('msg_class', 'alert-danger');
        }
        return redirect('admin/welcome');
    }

////////////////////////////////////////////////////////////////////////////////////		
    public function logout() {
        $this->session->unset_userdata('id'); //how  to unset session
        return redirect('welcome_view');
    }

}
?>